package u.pankratova.hw6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import u.pankratova.hw6.dao.BookDAO;
import u.pankratova.hw6.dao.ClientDAO;
import u.pankratova.hw6.model.Book;
import u.pankratova.hw6.model.Client;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class Hw6Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(Hw6Application.class, args);

        ClientDAO clientDAO = applicationContext.getBean(ClientDAO.class);
        BookDAO bookDao = applicationContext.getBean(BookDAO.class);

        Client client1 = new Client("Sidorov", "Ivan", LocalDate.of(1801, 11, 7), "89101482511", "some@mail.ru", new String[]{"Отверженные", "Морфий"});
        Client client2 = new Client("Petrov", "Max", LocalDate.of(2010, 01, 17), "89867548512", "some@internet.ru", new String[]{"Собор Парижской Богоматери", "Человек, который смеётся"});

        Book book1 = new Book("Отверженные", "Виктор Гюго", Date.valueOf("1900-01-14"));
        Book book2 = new Book("Морфий", "Булгаков" ,Date.valueOf("1876-04-4"));
        Book book3 = new Book("Собор Парижской Богоматери", "Виктор Гюго", Date.valueOf("2010-04-07"));
        Book book4 = new Book("Человек, который смеётся", "Виктор Гюго", Date.valueOf("1911-11-11"));

        clientDAO.addUser(client1);
        clientDAO.addUser(client2);
        bookDao.addBook(book1);
        bookDao.addBook(book2);
        bookDao.addBook(book3);
        bookDao.addBook(book4);

        List<Book> clientBook = bookDao.getAllUserBooks(clientDAO.getUserByPhone("89101482511"));

        System.out.println(clientBook);
    }
}
