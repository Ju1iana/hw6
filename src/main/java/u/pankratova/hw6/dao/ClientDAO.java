package u.pankratova.hw6.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import u.pankratova.hw6.mapper.ClientMapper;
import u.pankratova.hw6.model.Client;


@Component
@Scope("prototype")
public class ClientDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int addUser(Client client){
        String sql = "INSERT INTO client (client_surname, client_name, client_birthday, client_phone, client_mail, client_book_list) VALUES (?, ?, ?, ?, ?, ?)";
        return jdbcTemplate.update(sql, client.getClient_surname(), client.getClient_name(), client.getClient_birthday(), client.getClient_phone(), client.getClient_mail(), client.getClient_book_list());
    }

    public Client getUserByPhone(String phone){
        String sql = "SELECT * FROM client WHERE client_phone = ?";
        return jdbcTemplate.query(sql, new ClientMapper(), phone).stream().findFirst().orElseThrow(() -> new RuntimeException("User doesn't exist"));
    }
}
