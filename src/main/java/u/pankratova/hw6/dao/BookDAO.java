package u.pankratova.hw6.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import u.pankratova.hw6.mapper.BookMapper;
import u.pankratova.hw6.model.Book;
import u.pankratova.hw6.model.Client;
import java.util.Arrays;
import java.util.List;

@Component
@Scope("prototype")
public class BookDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int addBook(Book book) {
        String sql = "INSERT INTO books(book_title, book_author, book_date_added) VALUES (?, ?, ?)";
        return jdbcTemplate.update(sql, book.getBook_name(), book.getBook_author(), book.getBook_date_added());
    }

        public Book getBookByName (String name){
            String sql = "SELECT * FROM books WHERE book_title = ?";
            return jdbcTemplate.query(sql, new BookMapper(), name).stream().findFirst().orElseThrow(() -> new RuntimeException("Book doesn't exist"));
    }


    public List<Book> getAllUserBooks(Client client) {
        String[] clientBooks = client.getClient_book_list();
        return Arrays.stream(clientBooks).map(this::getBookByName).toList();
    }
}
