package u.pankratova.hw6.mapper;

import org.springframework.jdbc.core.RowMapper;
import u.pankratova.hw6.model.Client;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientMapper implements RowMapper<Client> {

    @Override
    public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
        Client client = new Client();
        client.setClient_surname(rs.getString("client_surname"));
        client.setClient_name(rs.getString("client_name"));
        client.setClient_mail(rs.getString("client_mail"));
        client.setClient_phone(rs.getString("client_phone"));
        client.setClient_book_list((String[]) rs.getArray("client_book_list").getArray());
        client.setClient_birthday(rs.getDate("client_birthday").toLocalDate());
        return client;
    }
}
