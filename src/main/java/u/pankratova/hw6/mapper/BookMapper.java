package u.pankratova.hw6.mapper;

import org.springframework.jdbc.core.RowMapper;
import u.pankratova.hw6.model.Book;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements RowMapper<Book> {
    @Override
    public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
        Book book = new Book();
        book.setId(rs.getLong("book_id"));
        book.setBook_name(rs.getString("book_title"));
        book.setBook_author(rs.getString("book_author"));
        book.setBook_date_added(rs.getDate("book_date_added"));
        return book;
    }

}
