package u.pankratova.hw6.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Data
public class Client {

    @Id
    private long client_id;
    private String client_surname;
    private String client_name;
    private LocalDate client_birthday;
    private String client_phone;
    private String client_mail;
    private String[] client_book_list;

    public Client() {
    }

    public Client(String surname, String name, LocalDate birthDate, String phone, String email, String[] books) {
        this.client_surname = surname;
        this.client_name = name;
        this.client_birthday = birthDate;
        this.client_phone = phone;
        this.client_mail = email;
        this.client_book_list = books;
    }

}
