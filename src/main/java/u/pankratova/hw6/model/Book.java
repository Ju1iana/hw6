package u.pankratova.hw6.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
public class Book {

    @Id
    private long id;

    private String book_name;
    private String book_author;
    private Date book_date_added;

    public Book(String name, String author, Date date_added) {
        this.book_name = name;
        this.book_author = author;
        this.book_date_added = date_added;
    }

    public Book() {
    }

}
