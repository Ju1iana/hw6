CREATE TABLE client
(
    client_id serial PRIMARY KEY,
    client_surname varchar (30),
    client_name varchar (20),
    client_birthday timestamp,
    client_phone varchar(100),
    client_mail varchar (50),
    client_book_list varchar
);
CREATE TABLE books
(
    book_id         serial primary key,
    book_title      varchar(100) not null,
    book_author     varchar(100) not null,
    book_date_added timestamp    not null
);

drop table client;
drop table books;




